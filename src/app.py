from fastapi import FastAPI

from apps import anagrams, devices


def create_app() -> FastAPI:
    app = FastAPI()

    app.include_router(anagrams.router, prefix="/anagrams")
    app.include_router(devices.router, prefix="/devices")

    @app.get("/healthcheck")
    def healthcheck() -> None:
        pass

    return app
