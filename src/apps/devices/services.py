import binascii
import os


def generate_device_id(length: int = 48) -> str:
    return str(binascii.b2a_hex(os.urandom(length)))
