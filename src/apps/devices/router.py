import random

from fastapi import APIRouter, Depends
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db.dependencies import get_session
from db.models import Device, Endpoint

from . import services

router = APIRouter(tags=["devices"])


@router.post(
    "/create-random-devices/",
    status_code=status.HTTP_201_CREATED,
)
async def create_random_devices(
    session: AsyncSession = Depends(get_session),
) -> None:
    for i in range(10):
        device = Device(
            device_id=services.generate_device_id(),
            device_type=random.choice(["emeter", "zigbee", "lora", "gsm"]),
        )
        if i % 2:
            device.endpoints = [Endpoint()]
        session.add(device)
    await session.commit()


@router.get(
    "/types/",
    response_model=dict[str, int],
)
async def device_type_list(
    session: AsyncSession = Depends(get_session),
) -> dict[str, int]:
    stmt = (
        select(
            Device.device_type,
            func.count(Device.id),
        )
        .group_by(Device.device_type)
        .where(~Device.endpoints.any())
    )
    result = (await session.execute(stmt)).all()
    return dict(result)
