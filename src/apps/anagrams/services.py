import collections

from fastapi import Depends
from redis import asyncio as aioredis

from dependencies import get_redis


class AnagramCounterService:
    __REDIS_KEY = "anagram-count"

    def __init__(
        self,
        redis: aioredis.Redis = Depends(get_redis),
    ):
        self.redis = redis

    async def get_count(self) -> int:
        cached = await self.redis.get(self.__REDIS_KEY)
        return int(cached) if cached else 0

    async def increment_count(self, increment: int = 1) -> None:
        count = await self.get_count()
        await self.redis.set(
            name=self.__REDIS_KEY,
            value=count + increment,
        )


def is_anagram(a: str, b: str) -> bool:
    return collections.Counter(a) == collections.Counter(b)
