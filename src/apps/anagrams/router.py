from fastapi import APIRouter, Depends

from . import services
from .schema import CheckAnagramResponseSchema, CheckAnagramSchema

router = APIRouter(
    tags=["anagrams"],
)


@router.post("/")
async def check_anagram(
    model: CheckAnagramSchema,
    count_service: services.AnagramCounterService = Depends(),
) -> CheckAnagramResponseSchema:
    is_anagram = services.is_anagram(model.a, model.b)
    if is_anagram:
        await count_service.increment_count()

    return CheckAnagramResponseSchema(
        is_anagram=is_anagram,
        count=await count_service.get_count(),
    )
