from pydantic import BaseModel


class CheckAnagramSchema(BaseModel):
    a: str
    b: str


class CheckAnagramResponseSchema(BaseModel):
    is_anagram: bool
    count: int
