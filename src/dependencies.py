from functools import lru_cache

from redis import asyncio as aioredis

import settings


@lru_cache
def get_redis() -> aioredis.Redis:
    return aioredis.Redis(
        host=settings.redis.host,
        port=settings.redis.port,
    )
