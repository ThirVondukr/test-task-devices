from sqlalchemy import BigInteger, Column, ForeignKey, String, Text
from sqlalchemy.orm import relationship

from db import Base


class Device(Base):
    __tablename__ = "devices"

    id = Column(BigInteger, primary_key=True)
    device_id = Column(String(200), nullable=False)
    device_type = Column(String(120), nullable=False)

    endpoints = relationship("Endpoint", back_populates="device")


class Endpoint(Base):
    __tablename__ = "endpoints"

    id = Column(BigInteger, primary_key=True)
    device_id = Column(ForeignKey("devices.id"), nullable=False)
    device = relationship("Device", back_populates="endpoints")
    comment = Column(Text)
