"""Add device and endpoint models

Revision ID: d18fe463c39b
Revises:
Create Date: 2022-05-17 21:24:54.813835

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "d18fe463c39b"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "devices",
        sa.Column("id", sa.BigInteger(), nullable=False),
        sa.Column("device_id", sa.String(length=200), nullable=False),
        sa.Column("device_type", sa.String(length=120), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "endpoints",
        sa.Column("id", sa.BigInteger(), nullable=False),
        sa.Column("device_id", sa.BigInteger(), nullable=False),
        sa.Column("comment", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["device_id"],
            ["devices.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("endpoints")
    op.drop_table("devices")
