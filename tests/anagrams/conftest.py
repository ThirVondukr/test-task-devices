import os
from typing import AsyncIterable

import pytest
from fastapi import FastAPI
from redis import asyncio as aioredis

import dependencies


@pytest.fixture(autouse=True)
async def redis_client(
    fastapi_app: FastAPI,
) -> AsyncIterable[aioredis.Redis]:
    client: aioredis.Redis = aioredis.Redis(host=os.getenv("REDIS_TEST_HOST", ""))

    async def inner() -> aioredis.Redis:
        return client

    async with client:
        fastapi_app.dependency_overrides[dependencies.get_redis] = lambda: inner
        yield client
        del fastapi_app.dependency_overrides[dependencies.get_redis]
