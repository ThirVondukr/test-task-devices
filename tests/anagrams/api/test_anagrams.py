import httpx
import pytest
from fastapi import FastAPI
from starlette import status

from apps.anagrams.services import AnagramCounterService


@pytest.fixture
async def endpoint_url(
    fastapi_app: FastAPI,
) -> str:
    return fastapi_app.url_path_for(name="check_anagram")


@pytest.fixture
async def current_count(anagram_service: AnagramCounterService) -> int:
    return await anagram_service.get_count()


@pytest.mark.parametrize(
    "a,b,is_anagram",
    (
        ("listen", "silent", True),
        ("silent", "listen", True),
        ("", "", True),
        ("not", "anagram", False),
    ),
)
async def test_anagrams(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    current_count: int,
    a: str,
    b: str,
    is_anagram: bool,
) -> None:
    for i in [1, 2]:
        pass
        response = await http_client.post(
            endpoint_url,
            json={
                "a": a,
                "b": b,
            },
        )

        assert response.status_code == status.HTTP_200_OK
        if is_anagram:
            assert response.json() == {
                "is_anagram": True,
                "count": current_count + i,
            }
        else:
            assert response.json() == {
                "is_anagram": False,
                "count": current_count,
            }
