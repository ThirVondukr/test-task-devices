import httpx
import pytest
from fastapi import FastAPI
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from starlette import status

from db.models import Device


@pytest.fixture
def endpoint_url(fastapi_app: FastAPI) -> str:
    return fastapi_app.url_path_for(name="create_random_devices")


async def test_create_random_devices(
    session: AsyncSession,
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == status.HTTP_201_CREATED

    stmt = select(Device).options(selectinload(Device.endpoints))
    devices = (await session.scalars(stmt)).all()
    assert len(devices) == 10
    assert sum(device.endpoints == [] for device in devices) == 5
