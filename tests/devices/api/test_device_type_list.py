import random

import httpx
import pytest
from fastapi import FastAPI
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from apps.devices.services import generate_device_id
from db.models import Device, Endpoint


@pytest.fixture
def endpoint_url(fastapi_app: FastAPI) -> str:
    return fastapi_app.url_path_for(name="device_type_list")


async def test_empty(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.get(endpoint_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {}


async def test_random(
    session: AsyncSession,
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    random.seed(42)
    device_types = [str(i) for i in range(10)]

    devices = []
    for _ in range(20):
        endpoints = [Endpoint()] if random.random() < 0.25 else []
        device = Device(
            endpoints=endpoints,
            device_id=generate_device_id(),
            device_type=random.choice(device_types),
        )
        devices.append(device)
    session.add_all(devices)
    await session.flush()

    unique_device_types = set(
        device.device_type for device in devices if not device.endpoints
    )

    response = await http_client.get(endpoint_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        device_type: sum(
            1
            for device in devices
            if device.device_type == device_type and not device.endpoints
        )
        for device_type in unique_device_types
    }
